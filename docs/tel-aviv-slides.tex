\documentclass[11pt]{beamer}

\usepackage{graphicx}
\usepackage{tipa}
\usepackage{lmodern}
\usepackage{fontspec}
\usepackage{booktabs}
\usepackage{multirow}
\usepackage[normalem]{ulem}
\useunder{\uline}{\ul}{}

\setmainfont{Doulos SIL}[ItalicFont=EBGaramond12-Italic]

\usetheme{Pittsburgh}

% \usefonttheme[stillsansseriflarge]{serif}
\usefonttheme{serif}

\setbeamertemplate{caption}[numbered]
\setbeamertemplate{navigation symbols}{}
%remove navigation symbols

\AtBeginSection[]
{
  \begin{frame}
    \frametitle{}
    \tableofcontents[currentsection]
  \end{frame}
}

\title{Areal sound change and the distributional typology of affricate richness in Eurasia}

\author{Dmitry Nikolaev \& Eitan Grossman}

\institute{Hebrew University of Jerusalem}

\date{Israel Phonology Circle meeting\\
Tel Aviv University, May 27, 2018}


\begin{document}

\frame{\titlepage}

\begin{frame}[t]\frametitle{Aims}
    
To investigate the distribution of Eurasian languages with rich or dense inventories of affricate segments, as a probe for the distributional typology (Bickel 2015) of complex consonant inventories. 

\vspace{5pt}

Distributional typologies aim to answer the question `what's where why when?' (Bickel 2007, 2015), i.e., to account for extant linguistic diversity in the world's languages, by accounting for the historical processes that lead to observed distributions of linguistic properties in space and time. 

\vspace{5pt}

On Greenbergian phonological typology, see the classic study by Maddieson (1984) and, recently, Gordon (2016).

\end{frame}

\begin{frame}[t]\frametitle{Affricate richness and affricate denseness}
    
We call a language \textit{affricate rich} if it has affricates at $3+$ places of articulation.\pause

\vspace{15pt}

We call a language \textit{affricate dense} if it has affricates at $3+$ places of articulation in the extended coronal range (everything between labial and velar areas).

\vspace{15pt}

E.g., a language with /pf, ts, kx/ is affricate rich, but not affricate dense, while a language with /ts, tʃ, tɕ/ is both affricate dense and affricate rich.

\end{frame}

\begin{frame}[t]\frametitle{Main claims}
    
While affricates are cross-linguistically common in the sound inventories of the world's languages, it is rare for languages to have multiple affricates.

\vspace{5pt}

Affricate-dense inventories are especially rare.

\end{frame}

\begin{frame}[t]\frametitle{Main claims}

\vspace{5pt}

Affricate-rich and affricate-dense inventories in Eurasia show clear areal patterning, which has two dimensions:

\begin{enumerate}
	\item Affricate-dense inventories tend to diffuse areally, whether through sound borrowing or areal sound change mostly leading to the emergence of retroflex affricates.
	\item Retroflex affricates tend to be lost when unsupported by neighboring languages that are also affricate dense.
\end{enumerate}

\end{frame}

\begin{frame}[t]\frametitle{Structure of the talk}
    
\tableofcontents

\end{frame}

\section{A little on the typology of affricates} % (fold)
\label{sec:a_little_on_the_typology_of_affricates}

\begin{frame}[t]\frametitle{Typological frequency of affricates}
    
Affricates are a very common class of consonants: 66.5\% of languages in the UPSID sample have one or more of them (Maddieson \& Precoda 1992).

\vspace{5pt}

Most typically /ts/.

\end{frame}

\begin{frame}[t]\frametitle{Typological frequency of affricates}
    
But on average they are limited in their total count in any given language: only 22.\% of languages in the UPSID have 3 or more affricates, only 15.7\% have 4 or more, and only 6.4\% (29) languages have 5 or more.

\vspace{5pt}

In contrast, 39.9\% of languages have 5 or more fricatives.

\end{frame}

\begin{frame}[fragile]\frametitle{What we are looking at}
    
Not segments but places of articulation\pause

\vspace{5pt}

Why:

\begin{table}[t]
\centering
\begin{tabular}{lllllll}
\hline
\begin{tabular}[c]{@{}l@{}}\# of places\\ of articulation\end{tabular} & 0 & 1 & 2 & 3 & 4 & 5 \\
\# of languages & 652 & 954 & 444 & 131 & 15 & 2 \\ \hline
\end{tabular}
\label{place-count}
\caption{Distribution of the number of different places of articulation for affricates in the languages of the world (based on PHOIBLE and EURPhon)}
\end{table}

Multiple places of articulation for affricates are very rare.

\end{frame}

\begin{frame}[fragile]\frametitle{Number of places for affricates in Eurasia}
    
\begin{table}[t]
\centering
\begin{tabular}{lllllll}
\hline
\begin{tabular}[c]{@{}l@{}}\# of places\\ of articulation\end{tabular} & 0 & 1 & 2 & 3 & 4 & 5 \\
\# of languages & 166 & 239 & 153 & 82 & 8 & 0 \\ \hline
\end{tabular}
\label{place-count-eurasia}
\caption{Distribution of the number of different places of articulation for affricates in the languages of Eurasia (based on PHOIBLE and EURPhon)}
\end{table}\pause

\vspace{3pt}

--$>$ More than half of the world's languages with $3$ or $4$ places of articulation for affricates are in Eurasia (which contains only 30\% of data-points in PHOIBLE + EURPhon).

\end{frame}

% section a_little_on_the_typology_of_affricates (end)

\section{Data} % (fold)
\label{sec:data}

\begin{frame}[t]\frametitle{}

\vspace{80pt}
    
A combined dataset of $648$ Eurasian languages from PHOIBLE (Moran, McCloy \& Wright 2014) and EURPhon (Nikolaev, Nikulin, and Kukhto 2015).

\end{frame}

% section data (end)

\section{Affricate-rich languages in Eurasia} % (fold)
\label{sec:affricate_rich_and_affricate_dense_languages_in_eurasia}

\begin{frame}[fragile]\frametitle{}

\begin{figure}[ht]
\centerline{
\includegraphics[width=1.1\textwidth]{images/affricate-rich-Eurasia.png}
}
\caption{Distribution of affricate-rich languages in Eurasia}
\label{fig:affricate-rich}
\end{figure}

\end{frame}

\begin{frame}[t]\frametitle{Four clear areas of affricate richness}
    
\begin{enumerate}
	\item The European area (consisting of Standard and Zurich German, several Slavic languages, Vlax Romani, and Gagauz).\pause
	\item The Caucasian area (languages from Abkhaz-Adyghe, Nakh-Daghestanian, and Kartvelian phyla).\pause
	\item The Hind Kush area (Pamir Iranian and Dardic languages, Burushaski, and one Western Tibetic variety).\pause
	\item The Eastern Himalayan area (Khams and Amdo Tibetic, Qiangic, rGyalrongic, Qinghai-Gansu Mongolic, varieties of Mandarin Chinese, Lolo-Burmese, Salar, Hmong-Njua, and possibly Daur).
\end{enumerate}

\end{frame}

% section affricate_rich_and_affricate_dense_languages_in_eurasia (end)

\section{A closer look at the areas} % (fold)
\label{sec:a_closer_look_at_the_areas}

\begin{frame}[fragile]\frametitle{European area: map}
    
\begin{figure}[ht]
\centerline{
\includegraphics[width=1.1\textwidth]{images/affricate-rich-Europe.pdf}
}
\caption{Distribution of affricate-rich languages in Europe}
\label{fig:affricate-rich-europe}
\end{figure}

\end{frame}

\begin{frame}[fragile]\frametitle{European area: inventories}

\begin{table}[]
\centerline{
{\footnotesize
\begin{tabular}{@{}llllllll@{}}
\toprule
\begin{tabular}[c]{@{}l@{}}Language\\ group\end{tabular} & Language & Labiodental & \begin{tabular}[c]{@{}l@{}}Denti-\\ alveolar\end{tabular} & \begin{tabular}[c]{@{}l@{}}Post-\\ alveolar\end{tabular} & \begin{tabular}[c]{@{}l@{}}Alveolo-\\ palatal\end{tabular} & Retroflex & Velar \\ \midrule
\multirow{2}{*}{Germanic} & \begin{tabular}[c]{@{}l@{}}Standard\\ German\end{tabular} & pf & ts & tʃ, dʒ &  &  &  \\
 & \begin{tabular}[c]{@{}l@{}}Zurich\\ German\end{tabular} & pf & ts & tʃ &  &  & kx \\ \cmidrule(l){2-8} 
\multirow{3}{*}{Slavic} & \begin{tabular}[c]{@{}l@{}}Lower\\ Sorbian\end{tabular} &  & ts & tʃ, dʒ & tɕ, dʑ &  &  \\
 & \begin{tabular}[c]{@{}l@{}}Standard\\ Polish\end{tabular} &  & ts, dz &  & tɕ, dʑ & ʈʂ, ɖʐ &  \\
 & Rusyn &  & ts, dz &  & tɕ, dʑ & ʈʂ, ɖʐ &  \\ \cmidrule(l){2-8} 
Indo-Aryan & \begin{tabular}[c]{@{}l@{}}Vlax\\ Romani\end{tabular} &  & ts & tʃʰ, dʒ & tɕʰ, dʑ &  &  \\ \cmidrule(l){2-8} 
Turkic & Gagauz &  & ts & tʃ & dʑ &  &  \\ \bottomrule
\end{tabular}
}
}
\end{table}

\end{frame}

\begin{frame}[t]\frametitle{European area: discussion}
    
Standard German and Zurich German are outliers: their affricate richness is due to peripheral segments (labiodental and velar affricates), absent in all other languages from this cluster. Therefore, they are not affricate dense.\pause

\vspace{3pt}

In Gagauz, the opposition in the place of articulation between /tʃ/ and /dʑ/ is augmented by that of VOT, so its status as affricate-rich is dubious.\pause

\vspace{3pt}

We are therefore left with some Slavic languages and Vlax Romani.\pause

\vspace{3pt}

While affricate richness in Vlax Romani may indeed be the result of contact, affricate richness in Slavic is as result of an interplay between phylogenetics (an array of to-be-palatalised segments) and contact influence.

\end{frame}

\begin{frame}[fragile]\frametitle{Caucasian area: map}
    
\begin{figure}[ht]
\centerline{
\includegraphics[width=1.1\textwidth]{images/affricate-rich-Caucasus.pdf}
}
\caption{Distribution of affricate-rich languages in the Caucasus}
\label{fig:affricate-rich-caucasus}
\end{figure}

\end{frame}

\begin{frame}[fragile]\frametitle{Caucasian area: inventories (sample)}

\begin{table}[]
{\tiny
\centerline{
\begin{tabular}{@{}lllllllll@{}}
\toprule
\begin{tabular}[c]{@{}l@{}}Language\\ group\end{tabular} & Language & \begin{tabular}[c]{@{}l@{}}Denti-\\ alveolar\end{tabular} & \begin{tabular}[c]{@{}l@{}}Post-\\ alveolar\end{tabular} & Lateral & Retroflex & \begin{tabular}[c]{@{}l@{}}Hissing-\\ hushing\end{tabular} & Velar & Uvular \\ \midrule
\begin{tabular}[c]{@{}l@{}}Abkhaz-\\ Adyghe\\ (Northwest\\ Caucasian)\end{tabular} & Adyghe & \begin{tabular}[c]{@{}l@{}}ts\textsuperscript{h}, ts',\\ dz\end{tabular} & t\textesh\textsuperscript{h}, t\textesh', d\textyogh &  & \begin{tabular}[c]{@{}l@{}}\textrtailt\textrtails\textsuperscript{h}, \textrtaild\textrtailz,\\ \textrtailt\textrtails'\end{tabular} & \begin{tabular}[c]{@{}l@{}}t\^s\textsuperscript{w}\textsuperscript{h}, t\^s\textsuperscript{h}, t\^s\textsuperscript{w}',\\ t\^s, 'd\^z, d\^z\textsuperscript{w}\end{tabular} &  &  \\
Kartvelian & Georgian & \begin{tabular}[c]{@{}l@{}}ts, ts',\\ dz\end{tabular} & t\textesh\textsuperscript{h}, t\textesh', d\textyogh &  &  &  &  & qχ \\
\begin{tabular}[c]{@{}l@{}}Nakh-\\ Daghestanian\\ (Northeast\\ Caucasian)\end{tabular} & Karata & \begin{tabular}[c]{@{}l@{}}ts\textsuperscript{h}, ts\textlengthmark\textsuperscript{h},\\ ts', ts\textlengthmark'\end{tabular} & \begin{tabular}[c]{@{}l@{}}t\textesh\textsuperscript{h}, t\textesh\textlengthmark\textsuperscript{h}, t\textesh',\\ t\textesh\textlengthmark', d\textyogh\end{tabular} & \begin{tabular}[c]{@{}l@{}}t\textbeltl\textlengthmark\textsuperscript{h}, t\textbeltl',\\ t\textbeltl\textlengthmark'\end{tabular} &  &  & \begin{tabular}[c]{@{}l@{}}kx\textlengthmark\textsuperscript{h},\\ kx\textlengthmark'\end{tabular} & \begin{tabular}[c]{@{}l@{}}q\textchi\textlengthmark\textsuperscript{h},\\ q\textchi\textlengthmark'\end{tabular} \\ \bottomrule
\end{tabular}
}
}
\end{table}

{\footnotesize Kabardian (closest relative to Adyghe) has alveolo-palatals instead of retroflexes.}

\end{frame}

\begin{frame}[t]\frametitle{Caucasian area: discussion}
    
The sheer prevalence of affricate richness in the area.

\vspace{3pt}

Many languages in the area have multiple affricates at the same place of articulation (unlike what is found in the European area).

\vspace{3pt}

Also, ejective affricates are very common in this area, but rare elsewhere in Eurasia.

\vspace{3pt}

So, the Caucasus looks like a bona fide linguistic area, cf. (Grawunder 2017).

\end{frame}

\begin{frame}[t]\frametitle{However}

Abkhaz-Adyghe languages are affricate dense, and have laminal affricates.\pause

\vspace{3pt}

The neighboring Kartvelian languages are not, and are affricate-rich only thanks to postvelar affricates.\pause

\vspace{3pt}

The Nakh-Daghestanian languages also have postvelar affricates, but no laminal affricates.

\end{frame}

\begin{frame}[t]\frametitle{In short...}

Each of the three groups of languages from different families has its own way of achieving affricate richness.\pause

\vspace{3pt}

It is hard to argue that the specific affricate richness observed is a contact-derived phenomenon in this region, since there is no evident segment borrowing or contact-induced sound changes.\pause

\vspace{3pt}

This means that the sources of affricate richness in the Caucasus are more or less unknown, since there is at present no explanation for contact-induced preferences for large or small inventories that are indifferent to the actual segments or contrasts involved.

\end{frame}

\begin{frame}[fragile]\frametitle{Hindu Kush area: map}
    
\begin{figure}[ht]
\centerline{
\includegraphics[width=1.1\textwidth]{images/affricate-rich-Hindu-Kush.pdf}
}
\caption{Distribution of affricate-rich languages in the Hindu Kush}
\label{fig:affricate-rich-hindukush}
\end{figure}

\end{frame}

\begin{frame}[fragile]\frametitle{Hindu Kush area:\\inventories (sample)}
    
\begin{table}[]
{\small
\centerline{
\begin{tabular}{@{}llllll@{}}
\toprule
\begin{tabular}[c]{@{}l@{}}Language\\ group\end{tabular} & Language & \begin{tabular}[c]{@{}l@{}}Denti-\\ alveolar\end{tabular} & \begin{tabular}[c]{@{}l@{}}Post-\\ alveolar\end{tabular} & \begin{tabular}[c]{@{}l@{}}Alveolo-\\ palatal\end{tabular} & Retroflex \\ \midrule
Burushaski & Hunza & ts, tsʰ &  & tɕ, tɕʰ, dʑ & ʈʂ, ʈʂʰ, ɖʐ \\
Iranian & Wakhi & ts, dz & tʃ, dʒ &  & ʈʂ, ɖʐ \\
Dardic & Khowar & ts, tsʰ, dz &  & tɕ, tɕʰ, dʑ & ʈʂ, ʈʂʰ, ɖʐ \\
Nuristani & Kati & ts, tsʲ, tsʷ, dz & tʃ, tʃʷ &  & tʃ, tʃʷ, dʒ \\
Tibetic & \begin{tabular}[c]{@{}l@{}}Western\\ Balti\end{tabular} & ts, tsʰ & tʃ, tʃʰ, dʒ &  & ʈʂ, ʈʂʰ, ɖʐ \\ \bottomrule
\end{tabular}
}
}
\end{table}

\end{frame}

\begin{frame}[t]\frametitle{Hindu Kush area: discussion}
   
Inventories of affricate-rich languages are uniform in this area.\pause

\vspace{5pt}

All are affricate dense, consisting of a dental series, a postalveolar (palato-alveolar or alveolo-palatal) series, and a retroflex series.

\end{frame}

\begin{frame}[fragile]\frametitle{Eastern Himalayan area: map}
    
\begin{figure}[ht]
\centerline{
\includegraphics[width=1.1\textwidth]{images/affricate-rich-E-Himalaya.pdf}
}
\caption{Distribution of affricate-rich languages in the Eastern Himalaya}
\label{fig:affricate-rich-himalaya}
\end{figure}

\end{frame}

\begin{frame}[fragile]\frametitle{Eastern Himalayan area:\\inventories (sample)}
    
	\begin{table}[ht]
		{\footnotesize
			\centerline{
			\begin{tabular}{@{}llllll@{}}
			\toprule
			\begin{tabular}[c]{@{}l@{}}Language\\ group\end{tabular} & Language & \begin{tabular}[c]{@{}l@{}}Denti-\\ alveolar\end{tabular} & \begin{tabular}[c]{@{}l@{}}Post-\\ alveolar\end{tabular} & \begin{tabular}[c]{@{}l@{}}Alveolo-\\ palatal\end{tabular} & Retroflex \\ \midrule
			Mongolic & Monggul & ts, tsʰ & tʃ, tʃʰ &  & ʈʂ, ʈʂʰ \\
			Qiangic & \begin{tabular}[c]{@{}l@{}}Northern\\ Qiang\end{tabular} & ts, tsʰ, dz &  & tɕ, tɕʰ, dʑ & ʈʂ, ʈʂʰ, ɖʐ \\
			rGyalrongic & Japhug & ts, tsʰ, dz, ⁿdz &  & tɕ, tɕʰ, dʑ, ⁿdʑ & ʈʂ, ʈʂʰ, ɖʐ, ⁿɖʐ \\
			Lolo-Burmese & Achang & ts, tsʰ &  & tɕ, tɕʰ & ʈʂ, ʈʂʰ \\
			Hmongic & Xong & ts, tsʰ, ⁿts, ⁿtsʰ &  & tɕ, tɕʰ, ⁿtɕ, ⁿtɕʰ & ʈʂ, ʈʂʰ, ⁿʈʂ, ⁿʈʂʰ \\ \bottomrule
			\end{tabular}
			}
		}
	\end{table}

\end{frame}

\begin{frame}[t]\frametitle{Eastern Himalayan area: discussion}
    
The largest in the sample, in terms of no. of languages and families.\pause

\vspace{5pt}

But also strikingly uniform: all are affricate dense and none involve peripheral places of articulation.\pause

\vspace{5pt}

Most of them share the `basic' set of dental, postalveolar, and retroflex affricates, similarly to the Hindu Kush languages.

\end{frame}

\begin{frame}[t]\frametitle{A unified history for Hindu Kush and Eastern Himalaya?}
    
It is plausible that there was a general diffusion of retroflex segments from South Asia, which could have led to the overlapping of the retroflex area with two distinct areas: the Hindu Kush area and the Eastern Himalayan area, both of which are characterised by large consonant inventories comprising several affricates, and both of which are high-altitude `accretion' zones (Nichols 1997).

\end{frame}

% section a_closer_look_at_the_areas (end)

\section{Affricate richness and retroflex affricates---areal sound-change processes} % (fold)
\label{sec:affricate_richness_and_retroflex_affricates_areal_sound_change_processes}

\begin{frame}[t]\frametitle{A shared feature of all affricate-rich areas}
    
Presence of \textit{retroflex affricates}:

\begin{table}[]
\centering
\begin{tabular}{@{}lll@{}}
\toprule
Affr. rich \textbackslash~Retrofl. affr. & $-$ & $+$ \\ \midrule
$-$ & 553 & 5 \\
$+$ & 28 & 62
\end{tabular}
\end{table}

($p$-value on Fisher's test $\approx 0$.)

\vspace{5pt}

---$>$ If a language is affricate rich, it is 70\% likely to have retroflex affricates. On the other hand, if a language has retroflex affricates, it is 92.5\% likely to be affricate rich.

\vspace{5pt}

I.e., \textit{retroflex affricates are extremely rare in non-affricate-rich languages.} (Russian being a prominent counter-example.)

\end{frame}

\begin{frame}[t]\frametitle{A historical conundrum}
    
Except for Burushaski, whose history is unknown, not a single affricate-rich language is known to have inherited affricate richness or even retroflex affricates from its respective proto-language.

\end{frame}

\begin{frame}[t]\frametitle{Sound changes leading to the\\emergence of retroflex segments:\\oversaturation theory}
    
Coronal oversaturation hypothesis was advanced by Hall (1997) to explain the emergence of retroflex fricatives in Indo-Aryan. Based on a (wrong) premise---`No language can contrast palatoalveolars and alveolopalatals' (Hall 1997: 205),---Hall hypothesised that at some stage in the development of Sanskrit, it acquired both /ʃ/ and /ɕ/ and then had to turn one of them into /ʂ/. This analysis was later applied to the history of Slavic languages by Rocho\'{n} (2001).

\end{frame}

\begin{frame}[t]\frametitle{Perceptual motivation\\for retroflex affricates}
    
The same scenario can be envisaged for the emergence of affricates. We now know that languages can contrast palato-alveolars and alveolo-palatals. 

\vspace{5pt}

Nevertheless, retroflex affricates are acoustically different from other coronal affricates (by virtue of having a lower spectral peak), and this makes them a good candidate for cue enhancement and enlargement of affricate inventories.

\end{frame}

\begin{frame}[t]\frametitle{However}
    
Retroflex affricates have a very narrow spatial distribution, coinciding with affricate-rich areas. 

\vspace{5pt}

Therefore, their emergence is most likely due to areally-induced sound-change processes: numerous sound changes leading to the emergence of retroflex affricates were attested exclusively in contemporary affricate-rich areas.

\end{frame}

\begin{frame}[t]\frametitle{Historical sources for retroflex affricates}
    
\begin{figure}[ht]
\centerline{
	\includegraphics[width=.9\textwidth]{images/paths-to-retroflex-affricates.png}
}
\caption{Paths to retroflex affricates in Hindu Kush and Eastern Himalayan areas}
\label{fig:affricate-rich-sound-changes}
\end{figure}

\end{frame}

\begin{frame}[t]\frametitle{Some more Tibetic sound changes from the Eastern Himalayan area}
    
\begin{enumerate}
	\item Retroflex affricates arising from clusters with rhotics
	\begin{itemize}
		\item k(\textsuperscript{h})r $>$ \textrtailt\textrtails(\textsuperscript{h}) (Themchen, rGyalthang, Kami)
		\item gr $>$ \textrtailt\textrtails~(Themchen, Kami, Zhongu)
		\item gr $>$ \textrtaild\textrtailz (Themchen, Kami, Zhongu)
		\item p(\textsuperscript{h})r $>$ \textrtailt\textrtails(\textsuperscript{h}) (Themchen, Kami, Zhongu)
		\item dr $>$ \textrtailt\textrtails~(Themchen, Kami, Zhongu)
		\item dr $>$ \textrtaild\textrtailz~(Themchen, Kami, Zhongu)
	\end{itemize}\pause
	\item Retroflex affricates arising from alveolo-palatal affricates before non-front vowels
	\begin{itemize}
		\item t\textctc(\textsuperscript{h}) $>$ \textrtailt\textrtails(\textsuperscript{h}) (rGyalthang, Melung)
		\item d\textctz $>$ \textrtaild\textrtailz~(rGyalthang, Melung)
	\end{itemize}
\end{enumerate}

\end{frame}

\begin{frame}[t]\frametitle{Other families}
 
Qinghai-Gansu Mongolic languages have either borrowed retroflex affricates from Mandarin Chinese or turned some of proto-affricates into retroflex affricates. Particulars of sound changes are unknown.\pause

\vspace{5pt}

The Turkic language Salar is also reported to have borrowed retroflex affricates from Mandarin Chinese.

\end{frame}

% section affricate_richness_and_retroflex_affricates_areal_sound_change_processes (end)

\section{Areally-dependent affricate loss} % (fold)
\label{sec:areally_dependent_affricate_loss}

\begin{frame}[t]\frametitle{Loss of retroflex affricates}
    
Recorded cases of retroflex-affricate loss confirm their dependence on areal support: \textit{languages tend to lose retroflex affricates outside affricate-rich areas}.

\end{frame}

\begin{frame}[t]\frametitle{Uralic family}
    
The Uralic family is the only Eurasian family for which a proto-retroflex affricate is confidently reconstructed. However, in most Uralic languages and subgroups, except for the isolated Central and Southern Selkup, and a tight cluster west of Urals (Eastern Khanty, Komi-Zyrian, Komi-Yodziak, Komi-Permyak, Udmurt, Eastern Mari, Erzya)---not a phylogenetic grouping---it has been lost.

\end{frame}

\begin{frame}[t]\frametitle{Serbo-Croatian}
    
In some varieties of Serbo-Croatian spoken in Croatia and Bosnia, the briefly emerged tripartite system /ts, t\textrtails, t\textctc/ was simplified into a bipartite one: /ts, t\textctc/.

\end{frame}

\begin{frame}[t]\frametitle{Lisu}
    
The most conservative Central Lisu varieties have retroflex affricates in three places of articulation (a plain voiceless series /ts, t\textrtails, t\textctc/ is accompanied by a voiceless aspirated and a voiced one). In Southern Lisu, they merged with alveolar fricatives and affricates, while in Northern Lisu they became allophones of alveolo-palatal fricatives and affricates.

\end{frame}

\begin{frame}[t]\frametitle{Retroflex-affricate loss in Chinese}
    
Retroflex affricates in Chinese are considered to have emerged between the Old Chinese and Middle Chinese periods. However, later they have been lost in most major dialect groups:

\begin{enumerate}
	\item Min
	\item Wu
	\item Hakka (except for one variety)
	\item Yue
\end{enumerate}

Also they were partly lost in Xiang, Gan, and Ping.

\end{frame}

\begin{frame}[t]\frametitle{Retroflex-affricate loss in Chinese: geography}
    
\begin{figure}[ht]
\centerline{
	\includegraphics[height=.6\textheight]{images/Chinese_dialect_groups.png}
}
\caption{Map of Chinese dialect groups (Wikimedia Commons)}
\label{fig:chinese-dialects}
\end{figure}

\end{frame}

\begin{frame}[t]\frametitle{Retroflex affricates in varieties of Mandarin Chinese}
    
The data on the varieties of Mandarin are incomplete, but the analyses of several varieties found in the literature (Xi'an, Hefei, Yangzhou, and Yunnan) suggest a similar pattern of progressive lost of retroflex affricates along the north-south and west-east axes.

\end{frame}

% section areally_dependent_affricate_loss (end)

\section{Conclusions} % (fold)
\label{sec:conclusions}

\begin{frame}[t]\frametitle{Conclusions}
    
The analysis of the distribution of languages with affricates at three or more places of articulation shows that there two general pathways that lead to their emergence.

\end{frame}

\begin{frame}[t]\frametitle{Conclusions}
    
Languages can:
\begin{enumerate}
	\item Develop affricates at peripheral places of articulation: labiodental and/or velar and post-velar (Standard and Swiss German, Kartvelian languages).\pause
	\item Develop affricate-dense inventories with three or more places of articulation in the coronal-palatal range.
\end{enumerate}

\end{frame}

\begin{frame}[t]\frametitle{Further observations}
    
\begin{enumerate}
	\item In the vast majority of cases, languages follow only one of these pathways.
	\item The pathway involving affricate-dense inventories is more widespread ($2$ areas in Eurasia + we located at least one more area outside Eurasia, cf. below).
	\item Affricate-dense inventories are extremely prone to contain retroflex affricates, which makes the latter a very strong correlate of affricate richness in general.
\end{enumerate}

\end{frame}

\begin{frame}[t]\frametitle{The analysis of sound change}
    
\begin{enumerate}
	\item The emergence of retroflex affricates is in the majority of cases the last step towards affricate denseness.
	\item Retroflex affricates are diachronically ‘marked’ in that they usually appear in inventories that already have affricates at two or more different places of articulation.
\end{enumerate}

\end{frame}

\begin{frame}[t]\frametitle{Two main scenarios}
    
\begin{enumerate}
	\item Tripartite fricative and affricate systems with a super-dense palatal region /s, ʃ, ɕ, ts, tʃ, tɕ/ saw the acoustically-based dissimilation of their middle elements, which led to the emergence of a more ‘grave’-sounding retroflex fricative and affricate series /s, ʂ, ɕ, ts, tʂ, tɕ/.
	\pagebreak
	\item The innovation of a series of retroflex affricates starting from a ‘maximally dispersed’ system consisting of /ts/ and /tɕ/. The sound changes involved in this process (simplication of consonant clusters, especially those with rhotics, yielding retroflex affricates and retroflexivisation of /tɕ/) are not attested outside these regions.
\end{enumerate}

\end{frame}

\begin{frame}[t]\frametitle{Conclusions, contd.}
    
Retroflex affricates therefore provide some of the most compelling evidence to date of areally-induced sound change: there is an impressive array of genera (and even phyla) whose languages underwent certain processes of sound-change that led to the emergence of retroflex affricates exclusively inside the Hindukush and the Eastern Himalayan regions.

\end{frame}

\begin{frame}[t]\frametitle{Conclusions, contd.}
    
Languages outside of areas where retroflex affricates are common tend to lose
them.

\vspace{5pt}

Corroborated by the history of Croatian and Bosnian varieties of Serbo-Croatian, most branches of the Uralic family, several varieties of Lisu, and a large number of Chinese varieties situated to the south and south-east of the core retroflex areas in the Eastern Himalayas and North China Plain.

\end{frame}

\begin{frame}[t]\frametitle{Future work}
    
Is this a world-wide tendency? Cf. the map of affricate richness in South America:

\begin{figure}[ht]
\centerline{
	\includegraphics[width=.7\textwidth]{images/South_America.png}
}
\caption{Affricate-rich languages in South America (SAPhon)}
\label{fig:s-america}
\end{figure}

\end{frame}

\begin{frame}[t]\frametitle{Affricate-rich inventories in South America}

{\small
\begin{table}[h]
\centering
\begin{tabular}{lll}
\hline
Language & Phylum & Affricate inventory \\ \hline
Ancash Quechua & Quechua & ts, t\textesh, \textrtailt\textrtails \\
Cams{\'a} & Isolate & ts, t\textesh, \textrtailt\textrtails \\
Candoshi-Shapra & Isolate & ts, t\textesh, \textrtailt\textrtails \\
Chamicuro & Arawak & ts, t\textesh, \textrtailt\textrtails \\
Chipaya & Uru-Chipaya & ts, t\textesh, \textrtailt\textrtails, ts', t\textesh', \textrtailt\textrtails', ts\textsuperscript{h}, t\textesh\textsuperscript{h}, \textrtailt\textrtails\textsuperscript{h} \\
Guambiano & Barbacoan & ts, t\textesh, \textrtailt\textrtails \\
Mat{\'i}s & Panoan & ts, t\textesh, \textrtailt\textrtails \\
Mats{\'e}s & Panoan & ts, t\textesh, \textrtailt\textrtails \\
Muniche & Isolate & ts, t\textesh, \textrtailt\textrtails \\
Y{\'a}nesha & Arawak & ts, t\textesh, \textrtailt\textrtails \\
Yine & Arawak & ts, t\textesh, c\c{c} \\
Shipibo & Panoan & ts, t\textesh, \textrtaild\textrtailz \\ \hline
\end{tabular}
\end{table}
}

\end{frame}

\begin{frame}[t]\frametitle{}
    
\vspace{100pt}

{\huge Thank you for your attention!}

\end{frame}

% section conclusions (end)

\end{document}