\beamer@sectionintoc {1}{A little on the typology of affricates}{8}{0}{1}
\beamer@sectionintoc {2}{Data}{15}{0}{2}
\beamer@sectionintoc {3}{Affricate-rich languages in Eurasia}{17}{0}{3}
\beamer@sectionintoc {4}{A closer look at the areas}{23}{0}{4}
\beamer@sectionintoc {5}{Affricate richness and retroflex affricates---areal sound-change processes}{49}{0}{5}
\beamer@sectionintoc {6}{Areally-dependent affricate loss}{60}{0}{6}
\beamer@sectionintoc {7}{Conclusions}{68}{0}{7}
